package edu.ib.springdataexample;

import edu.ib.springdataexample.repository.UserDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
//jak debugowałam to, to z tokena wychodziło jakieś dziwne hasło
public class JwtFilter extends BasicAuthenticationFilter {

    public JwtFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader("Authorization");

        UsernamePasswordAuthenticationToken authResult = null;
        try{
            authResult = getAuthenticationByToken(header);

        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
        SecurityContextHolder.getContext().setAuthentication(authResult);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthenticationByToken(String header) {

        Jws<Claims> claimsJws = null;
        for (UserDto userDto: DbMockData.allUsers) {
            try {
                claimsJws = Jwts.parser().setSigningKey(userDto.getPasswordHash())
                        .parseClaimsJws(header.replace("Bearer ", ""));
            } catch (Exception e){
                continue;
            }
            break;
        }
        if (claimsJws==null) return null;
        String role = claimsJws.getBody().get("role").toString();
        String username = claimsJws.getBody().get("sub").toString();
        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = Collections.singleton(new SimpleGrantedAuthority(role));
        return new UsernamePasswordAuthenticationToken(username, null, simpleGrantedAuthorities);

    }
}

package edu.ib.springdataexample;

import edu.ib.springdataexample.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepo orderRepository;
    private CustomerRepo customerRepository;
    private UserRepo userRepository;
    private UserDtoBuilder builder;
    public static Iterable<UserDto> allUsers;

    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepo orderRepository, CustomerRepo customerRepository, UserRepo userRepository, UserDtoBuilder builder) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
        this.builder = builder;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Komiks: Daredevil - Nieustraszony Tom 1", 109.95f,true);
        Product product1 = new Product("Gra: Eksplodujące kotki", 139.99f, true);
        Product product2 = new Product("Gra: Everdell", 249.99f, false);
        Customer customer = new Customer("Julia Różycka", "Wrocław ul. Sezamkowa 15");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }};
        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");
        userRepository.save(builder.buildUser(new User("user","pass","ROLE_CUSTOMER")));
        userRepository.save(builder.buildUser(new User("admin","pass","ROLE_ADMIN")));
        allUsers = userRepository.findAll();
        productRepository.save(product);
        productRepository.save(product1);
        productRepository.save(product2);
        customerRepository.save(customer);
        orderRepository.save(order);
    }
}

package edu.ib.springdataexample.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserDtoBuilder {
    PasswordEncoder passwordEncoder;

    @Autowired
    public UserDtoBuilder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto buildUser(User user) {
        String password = user.getPassword();
        String passwordHash = passwordEncoder.encode(password);

        return new UserDto(user.getName(), passwordHash, user.getRole());
    }
}

package edu.ib.springdataexample.controller;


import edu.ib.springdataexample.DbMockData;
import edu.ib.springdataexample.repository.PasswordEncoderConfig;
import edu.ib.springdataexample.repository.User;
import edu.ib.springdataexample.repository.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class TokenController {
    private PasswordEncoderConfig passwordEncoderConfig;
    private String password;
    private String role;

    @Autowired
    public TokenController(PasswordEncoderConfig passwordEncoderConfig) {
        this.passwordEncoderConfig = passwordEncoderConfig;
    }

    @PostMapping("/api/getToken")
    public String getToken(@RequestBody User user) {

        long currentTime = System.currentTimeMillis();
        if (isPasswordCorrect(user)) {
            return Jwts.builder()
                    .setSubject(user.getName())
                    .claim("role", role)
                    .setIssuedAt(new Date(currentTime))
                    .setExpiration(new Date(currentTime + 20000))
                    .signWith(SignatureAlgorithm.HS256, password)
                    .compact();
        }
        return "Wrong data";
    }

    private boolean isPasswordCorrect(User user) {
        for (UserDto userDto: DbMockData.allUsers){
            if (userDto.getName().equals(user.getName())){
                if (passwordEncoderConfig.passwordEncoder().matches(user.getPassword(), userDto.getPasswordHash())){
                    role = userDto.getRole();
                    password = userDto.getPasswordHash();
                    return true;
                }
            }
        }
        return false;
    }
}

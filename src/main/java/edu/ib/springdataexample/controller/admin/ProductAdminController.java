package edu.ib.springdataexample.controller.admin;

import edu.ib.springdataexample.controller.customer.ProductController;
import edu.ib.springdataexample.repository.Product;
import edu.ib.springdataexample.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/product")
public class ProductAdminController extends ProductController {

    @Autowired
    public ProductAdminController(ProductService productService) {
        super(productService);
    }

    @PostMapping
    public Product addProduct(@RequestBody Product product) {
        return productService.save(product);
    }

    @PutMapping
    public Product putProduct(@RequestBody Product product, @RequestParam Long id) {
        productService.findById(id).orElseThrow(IllegalArgumentException::new);
        product.setId_prod(id);
        return addProduct(product);
    }

    @RequestMapping(method = RequestMethod.PATCH)
    public Product updateProduct(@RequestParam Long id, @RequestBody Product product) {
        Product product1 = getById(id).orElseThrow(IllegalArgumentException::new);
        if (product.getName()!=null) product1.setName(product.getName());
        if (product.getPrice()!=0.0f) product1.setPrice(product.getPrice());
        return  productService.save(product1);
    }
}

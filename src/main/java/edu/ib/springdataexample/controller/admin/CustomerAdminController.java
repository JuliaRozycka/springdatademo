package edu.ib.springdataexample.controller.admin;


import edu.ib.springdataexample.controller.customer.CustomerController;
import edu.ib.springdataexample.repository.Customer;
import edu.ib.springdataexample.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/admin/customer")
public class CustomerAdminController extends CustomerController {


    @Autowired
    public CustomerAdminController(CustomerService customerService) {
        super(customerService);
    }

    @PostMapping
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerService.save(customer);
    }

    @PutMapping
    public Customer putCustomer(@RequestBody Customer customer, @RequestParam Long id) {
        customerService.findById(id).orElseThrow(IllegalArgumentException::new);
        customer.setId(id);
        return addCustomer(customer);
    }

    @RequestMapping(method = RequestMethod.PATCH)
    public Customer updateCustomer(@RequestParam Long id, @RequestBody Customer customer) {
        Customer customer1 = getById(id).orElseThrow(IllegalArgumentException::new);
        if (customer.getName()!=null) customer1.setName(customer.getName());
        if (customer.getAddress()!=null) customer1.setAddress(customer.getAddress());
        return  customerService.save(customer1);
    }

}

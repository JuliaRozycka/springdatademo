package edu.ib.springdataexample.controller.admin;

import edu.ib.springdataexample.controller.customer.OrderController;
import edu.ib.springdataexample.repository.Customer;
import edu.ib.springdataexample.repository.Order;
import edu.ib.springdataexample.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/order")
public class OrderAdminController extends OrderController {

    @Autowired
    public OrderAdminController(OrderService orderService) {
        super(orderService);
    }

    @PostMapping
    public Order addOrder(@RequestBody Order order) {
        return orderService.save(order);
    }

    @PutMapping
    public Order putOrder(@RequestBody Order order, @RequestParam Long id) {
        orderService.findById(id).orElseThrow(IllegalArgumentException::new);
        order.setId_order(id);
        return addOrder(order);
    }

    @RequestMapping(method = RequestMethod.PATCH)
    public Order updateOrder(@RequestParam Long id, @RequestBody Order order) {
        Order order1 = getById(id).orElseThrow(IllegalArgumentException::new);
        if (order.getStatus()!=null) order1.setStatus(order.getStatus());
        if (order.getCustomer()!=null) order1.setCustomer(order.getCustomer());
        if (order.getProducts()!=null) order1.setProducts(order.getProducts());
        if (order.getPlaceDate()!=null) order1.setPlaceDate(order.getPlaceDate());
        return  orderService.save(order1);
    }

}

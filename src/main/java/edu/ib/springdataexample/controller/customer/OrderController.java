package edu.ib.springdataexample.controller.customer;

import edu.ib.springdataexample.repository.Order;
import edu.ib.springdataexample.repository.Product;
import edu.ib.springdataexample.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    protected OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/all")
    public Iterable<Order> getAll() {
        return orderService.findAll();
    }

    @GetMapping
    public Optional<Order> getById(@RequestParam Long id) {
        return orderService.findById(id);
    }
}

package edu.ib.springdataexample.controller.customer;


import edu.ib.springdataexample.repository.Customer;
import edu.ib.springdataexample.repository.Product;
import edu.ib.springdataexample.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    protected CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    public Iterable<Customer> getAll() {
        return customerService.findAll();
    }

    @GetMapping
    public Optional<Customer> getById(@RequestParam Long id) {
        return customerService.findById(id);
    }

}

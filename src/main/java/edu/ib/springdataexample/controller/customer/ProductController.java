package edu.ib.springdataexample.controller.customer;

import edu.ib.springdataexample.repository.Product;
import edu.ib.springdataexample.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    protected ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    public Iterable<Product> getAll() {
        return productService.findAll();
    }

    @GetMapping
    public Optional<Product> getById(@RequestParam Long id) {
        return productService.findById(id);
    }
}

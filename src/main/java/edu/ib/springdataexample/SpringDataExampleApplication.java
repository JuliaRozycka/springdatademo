package edu.ib.springdataexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class SpringDataExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataExampleApplication.class, args);
    }

}

package edu.ib.springdataexample.service;

import edu.ib.springdataexample.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public Iterable<UserDto> findAll() {
        return userRepo.findAll();
    }

    public Optional<UserDto> findById(Long id) {
        return userRepo.findById(id);
    }

    public UserDto save(UserDto userDto) {
        return userRepo.save(userDto);
    }
}

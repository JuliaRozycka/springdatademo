package edu.ib.springdataexample.service;


import edu.ib.springdataexample.repository.Order;
import edu.ib.springdataexample.repository.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderService {
    private OrderRepo orderRepo;

    @Autowired
    public OrderService(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }

    public Optional<Order> findById(Long id) {
        return orderRepo.findById(id);
    }

    public Iterable<Order> findAll() {
        return orderRepo.findAll();
    }

    public Order save(Order product) {
        return orderRepo.save(product);
    }
}

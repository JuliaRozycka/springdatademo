package edu.ib.springdataexample.service;

import edu.ib.springdataexample.repository.Customer;
import edu.ib.springdataexample.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {
    private CustomerRepo customerRepo;

    @Autowired
    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public Optional<Customer> findById(Long id) {
        return customerRepo.findById(id);
    }

    public Iterable<Customer> findAll() {
        return customerRepo.findAll();
    }

    public Customer save(Customer product) {
        return customerRepo.save(product);
    }

}

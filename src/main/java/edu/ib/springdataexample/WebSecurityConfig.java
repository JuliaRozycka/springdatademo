package edu.ib.springdataexample;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/api/getToken")
                .antMatchers("/api/product")
                .antMatchers("/api/product/all")
                .antMatchers("/api/order")
                .antMatchers("/api/order/all");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/customer").hasRole("CUSTOMER")
                .antMatchers("/api/customer/all").hasRole("CUSTOMER")
                .antMatchers("/api/admin/*").hasRole("ADMIN")
                .and()
                .addFilter(new JwtFilter(authenticationManager()));

        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
}

